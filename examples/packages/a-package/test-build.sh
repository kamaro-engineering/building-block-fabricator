#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

rm -rf build
mkdir build
cd build

CMAKE_PREFIX_PATH=../../rodos/install cmake -DCMAKE_INSTALL_PREFIX:PATH=../../rodos/install -DCMAKE_TOOLCHAIN_FILE=../rodos/rodos/cmake/port/linux-makecontext.cmake -DEXECUTABLE=ON ..
make all
make install
