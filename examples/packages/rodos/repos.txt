---
repositories:
  rodos:
    type: git
    url: https://gitlab.com/rodos/rodos.git
    version: master
