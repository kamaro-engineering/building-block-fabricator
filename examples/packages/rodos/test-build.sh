#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

rm -rf install
mkdir install

rm -rf build
mkdir build
cd build

cmake -DCMAKE_INSTALL_PREFIX:PATH=$DIR/install -DCMAKE_TOOLCHAIN_FILE=../rodos/cmake/port/linux-makecontext.cmake -DEXECUTABLE=ON ..
make all
make install
