from setuptools import setup, find_packages

setup(
    name='building-block-factory',
    version='0.1',
    description='community supported RODOS building block manager',
    url='',
    author='Nicolas Kessler',
    author_email='nicolas.kessler@kamaro-engineering.de',
    license='Apache License Version 2',
    packages=find_packages(),
    install_requires=[
        "pyyml",
    ],
    entry_points={
        'console_scripts': [
            'example=building-block-fabricator.demo:main',
        ],
    },
    zip_safe=False
)
